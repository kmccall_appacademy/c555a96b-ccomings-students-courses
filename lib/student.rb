class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{first_name} #{last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    return if courses.include?(course)
    raise "Error" if pending_conflict?(course)
    self.courses << course
    course.students << self
  end

  def course_load
    load_hash = Hash.new(0)
    self.courses.each do |course|
      load_hash[course.department] += course.credits
    end
    load_hash
  end

  def pending_conflict?(new_course)
    self.courses.any? do |course|
      new_course.conflicts_with?(course)
    end
  end
end
